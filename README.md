# Hangman game

## Installation 

Requirements:

* Linux or Macos. It should also work on [WSL](https://docs.microsoft.com/en-us/windows/wsl/about) 
    in Windows 10+.
* Python 3.7 (tested). Might work with other 3.* but I have not tested with them.
* GNU Make -- for running automation tasks. If you don't have this one for some reason, you can try running shell 
    commands specified in the `Makefile`.

Recommended way:

    $ make install
    
This should create and activate a python virtual environment.
    
## Playing

Game uses a command line interface. To start it execute:

    $ make play
    
## Running tests

To run unit test:

    $ make test
    
To run them with HTML coverage report:

    $ make test-coverage
    
Report will be generated in `./tests/coverage-report/`. 
You can open it manually in a browser (`index.html`) or using: 

    $ make open-coverage-report

## Purpose

Practice coding in Python.

## Checklist

Features:

* [x] high scores
* [x] configurable game logic
* [x] playthrough with limited tries
* [x] masked word output 
* [x] random word selection from a pre-defined list of words

Tech:

* [x] task automation with `make`
* [x] CI pipeline
* [x] unit tests * missing some tests for the high scores board
* [ ] acceptance tests

## Todos and ideas

* investigate [tox](https://tox.readthedocs.io/)
* web api to check game logic and control separation
* dockerize :) or build a dist binary