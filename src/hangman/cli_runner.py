"""Handles user's interaction with the game.
"""

from hangman.game import Hangman
from high_scores_board.file_repository import HighScoreBoardFileRepository
from high_scores_board.high_score_board import HighScoreBoard


def ask_to_start(question: str) -> bool:
    """Ask the user to start the game. Will take input and process it
    to get the answer.
    :param question: str
    :return: bool
    """
    while "the answer is invalid":
        reply = str(input(question + ' (y/n): ')).lower().strip()
        if reply[:1] == 'y':
            return True
        if reply[:1] == 'n':
            return False


def process_users_guess(prompt_message: str) -> str:
    """Process user's guess by validating invalid ones and returning a valid one.
    :param prompt_message: str
    :return: str
    """
    while "user's guess is not valid":
        users_guess = str(input(prompt_message)).lower().strip()
        if users_guess == "":
            continue
        if len(users_guess) > 1:
            print("You can only guess a single letter at a time. "
                  + f"Your tried: {users_guess}. Try again.")
            continue
        return users_guess[0]


def game_loop(game: Hangman) -> bool:
    """Responsible for keeping the game going.
    :type game: Hangman
    :return: bool True if the user won, False if she/he didn't.
    """
    game.start()
    while not game.is_game_over():
        print(f"Current status: ", *list(game.get_mask()), f". Tries left: {game.get_tries_left()}")
        current_guess = process_users_guess("Guess a single letter: ")
        print(f"Your guess is \"{current_guess}\".")
        if game.guess(current_guess):
            print(f"You have guessed correctly!")
        else:
            print(f"You have guessed incorrectly")

    if game.is_solved():
        print(f"It's indeed \"{game.get_mask()}\". Congratulations, you have won!")
        return True
    print(f"Sorry, you have lost. The word was \"{game.get_word()}\" Better luck next time!")
    return False


def start_the_game(game_logic: Hangman) -> int:
    """ Initiate a new game session. Will keep the game going until user interrupts
    it or decides to stop playing.
    :type game_logic: Hangman
    """
    wins, attempts = 0, 0
    while ask_to_start('Do you want to play Hangman?'):
        attempts += 1
        wins += int(game_loop(game_logic))
        print(f"You have won {wins} out of {attempts} attempt(s)")
    return wins


def update_high_score(repo, player: str, score: int) -> HighScoreBoard:
    """Updates the high score board with new entry and preserves it.
    :param repo: repository of high score boards
    :param player: str
    :param score: int
    :return: HighScoreBoard
    """
    board = repo.get()
    board.add(player, score)
    repo.save(board)
    return board


if __name__ == "__main__":
    score = start_the_game(Hangman())

    if score > 0:
        player_name = str(input('Enter your name to feature on the high score board: '))
        repository = HighScoreBoardFileRepository(storage_path='./data/high_scores')
        board = update_high_score(repository, player_name, score)
        print('Current standings', board, sep=f"\n{12*'='}\n")
    print('Bye!')
