"""Contains core game logic.
"""
import random


class Hangman:
    """Hangman game logic. The objective of the game is to guess a word
       letter-by-letter. Player is given a defined number of invalid
       guesses -- tries before the game ends. To win, player needs
       to uncover the masked word before she/he runs ouf of tries.
    """
    def __init__(self, max_tries=6, word_list_file_path="./src/hangman/word_lists/words.txt"):
        """
        :param max_tries: int max number of incorrect attempts at guessing the word
        :param word_list_file_path: str path the the file with words that can be guessed
        """
        self.tries = 0
        self.max_tries = max_tries
        self.word_list_file_path = word_list_file_path
        self.word = ""
        self.mask = "_"
        self.start()

    def start(self):
        """Initializes the game.
        """
        self.word = self.pick_a_word()
        self.mask = "_" * len(self.word)

    def guess(self, guess: str) -> bool:
        """Implements player's guess.
        Returns bool to tell if the guess was correct or not
        :param guess: str with a single letter that will be checked against the word being guessed.
        :return: bool True if the guess was correct, False otherwise.
        """
        if self.is_game_over():
            raise RuntimeError("Game is over. Cannot guess anymore.")
        if guess in self.word:
            mask = list(self.mask)
            for index, letter in enumerate(self.word):
                if letter == guess:
                    mask[index] = letter
                    self.mask = ''.join(mask)
            return True
        self.tries += 1
        return False

    def get_tries_left(self) -> int:
        """ Return how many tries the player has left.
        :return: int
        """
        return self.max_tries - self.tries

    def is_game_over(self) -> bool:
        """Determine if the game is over.
        :return: bool
        """
        return self.tries >= self.max_tries or self.is_solved()

    def is_solved(self) -> bool:
        """Determines if the user already guessed the word
        :return: bool
        """
        return self.word == self.mask

    def get_word(self) -> str:
        """Return the word being guessed.
        :return: str
        """
        return self.word

    def get_mask(self) -> str:
        """Get the masked version of the word being guessed.
        :return: str
        """
        return self.mask

    def pick_a_word(self) -> str:
        """Sets a random word to guess based on the dictionary file
        :return: str
        """
        with open(self.word_list_file_path) as file:
            words = file.read().split()
            return random.choice(words)
