"""Handles persistence and reads to high scores board storage.
"""

from high_scores_board.high_score_board import HighScoreBoard


class HighScoreBoardFileRepository:
    """Handles persistence and reads to high scores board storage.
    """

    def __init__(self, storage_path='./data/high_scores'):
        self.storage_path = storage_path

    def get(self) -> HighScoreBoard:
        """Load the board from file storage.
        :return: HighScoreBoard
        """
        with open(self.storage_path, 'r') as file:
            data = file.read()
            return HighScoreBoard.create_from_str(data)

    def save(self, board: HighScoreBoard):
        """Save the board to file storage.
        :param board:
        """
        with open(self.storage_path, 'w') as file:
            file.write(str(board))
