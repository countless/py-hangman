"""Keeps track of play-through high scores.
"""


class HighScoreBoard:
    """Represents a high score board.
    """

    def __init__(self, board: list):
        """
        :param board:
        """
        self.board = board
        self.sort_by_high_score()

    def sort_by_high_score(self):
        """Sort the board by player's high score.
        :return: None
        """
        if not self.board:
            return
        self.board = sorted(self.board, key=lambda entry: int(entry[1]), reverse=True)

    def add(self, player: str, score: int):
        """Add a new entry to the board:20
        :param score: int
        :param player: str
        :return: None
        """
        if player in self:
            self[player] = score
        else:
            self.board.append((player, int(score)))
        self.sort_by_high_score()

    def __setitem__(self, key, value):
        for position, entry in enumerate(self.board):
            if entry[0] == key:
                self.board[position] = (key, value)
                return
        self.board[key] = value

    def get_top_scorer(self):
        """Returns the highest score holder name.
        :return: str
        """
        return self.board[0][0]

    def __iter__(self):
        return iter(self.board)

    def __str__(self) -> str:
        return '\n'.join([f"{entry[0]},{str(entry[1])}" for entry in self.board])

    def __getitem__(self, item):
        for entry in self.board:
            # if lookup item is a player then only return the score
            if item == entry[0]:
                return entry[1]
        return self.board[item]

    def __contains__(self, player: str):
        for entry in self.board:
            if player in entry:
                return True
        return False

    def __len__(self):
        return len(self.board)

    @staticmethod
    def create_from_str(string: str):
        """Create an instance of the board from a str representation.
        :param string:
        :return:
        """
        if string == "":
            return HighScoreBoard(list())
        lines = string.split('\n')
        board = [line.strip().split(',') for line in lines]
        return HighScoreBoard(board)
