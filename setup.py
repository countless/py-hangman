from setuptools import setup, find_packages

setup(
    name='py-hangman',
    version='0.0.1',
    license='MIT',
    description='Hangman game',

    author='countless-integers',
    author_email='totcha@gazeta.pl',

    packages=find_packages(where='src'),
    package_dir={'': 'src'},

    install_requires=['pytest-runner', 'pytest', 'pytest-cov'],

    entry_points={
        'console_scripts': [
            'hangman = hangman.cli_runner:start_the_game',
        ],
    },
)