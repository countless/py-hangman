default: help

coverage-report-dir = ./tests/coverage-report

## See help info
help:
	@echo "Usage: make <target>"
	@echo "Available targets:"
	@awk '/^##/ {c=$$0}; /^[a-zA-Z_-]+:/ {gsub(":$$", "", $$1); gsub(/^#+/, "", c); printf "\033[36m%-30s\033[0m %s\n", $$1, c}; /^([^#]|$$)/ {c=""}' $(MAKEFILE_LIST)

## Set-up the virtual env and activate it
	python -m venv venv --prompt py-hangman
	source venv/bin/activate

## Install all dependencies
install:
	pip install -e .

## Run unit tests
test:
	pytest -v ./tests

## Run unit tests with coverage
test-coverage:
	pytest -v --cov=./src/ ./tests
	coverage html --directory $(coverage-report-dir)

## open coverage report
open-coverage-report:
	open $(coverage-report-dir)/index.html 2>/dev/null || xdg-open $(coverage-report-dir)/index.html 2>/dev/null

## Run the game
play:
	python3 ./src/hangman/cli_runner.py

## Run static code analysis
static-analysis:
	pylint src

## Run all tasks required for CI delivery
delivery:
	pytest -v --junit-xml=./test-reports/junit.xml ./tests
