"""Unit tests for the high scores class.
"""
import random

import pytest

from high_scores_board.high_score_board import HighScoreBoard


@pytest.fixture
def board() -> HighScoreBoard:
    board = [
        ('gerald', 10),
        ('ciri', 20),
        ('zoltan', 5),
    ]
    return HighScoreBoard(board)


def test_it_can_add_new_players(board: HighScoreBoard):
    assert len(board) == 3
    board.add('triss', random.randint(0, 10))
    assert len(board) == 4


def test_it_can_return_the_highest_scorer(board: HighScoreBoard):
    assert board.get_top_scorer() == 'ciri'
    assert 'ciri' in board[0]


def test_it_can_represent_itself_as_string(board: HighScoreBoard):
    assert str(board) == "ciri,20\ngerald,10\nzoltan,5"


def test_it_can_be_created_from_str(board: HighScoreBoard):
    restored_board = HighScoreBoard.create_from_str("ciri,20\ngerald,10\nzoltan,5")
    assert isinstance(restored_board, HighScoreBoard)
    assert restored_board.get_top_scorer() == board.get_top_scorer()


def test_it_can_check_if_a_player_is_featured_on_the_board(board: HighScoreBoard):
    assert 'ciri' in board
    assert 'dandelion' not in board


def test_it_can_get_player_score(board: HighScoreBoard):
    assert board['ciri'] == 20


def test_it_can_update_players_score(board: HighScoreBoard):
    zoltan = 'zoltan'
    assert board[zoltan] == 5
    board.add(zoltan, 100)
    assert board[zoltan] == 100
    assert board.get_top_scorer() == zoltan
    occurances = 0
    for entry in board:
        if entry[0] == zoltan:
            occurances += 1
    assert occurances == 1
