"""
Unit tests of the hangman class
"""
import pytest

from hangman.game import Hangman


@pytest.fixture
def game() -> Hangman:
    return Hangman(
        max_tries=3,
        word_list_file_path='tests/hangman/word_lists/single_word.txt'
    )


def test_it_can_pick_a_word(game: Hangman):
    word = game.pick_a_word()
    assert isinstance(word, str)
    assert word == game.get_word()


def test_it_can_be_solved(game: Hangman):
    game.guess("c")
    assert game.is_solved() is False
    game.guess("a")
    assert game.is_solved() is False
    game.guess("k")
    assert game.is_solved() is False
    game.guess("e")
    assert game.is_solved() is True


def test_it_will_not_let_you_guess_forever(game: Hangman):
    assert game.is_solved() is False
    assert game.is_game_over() is False
    game.guess("x")
    game.guess("x")
    game.guess("x")
    assert game.is_solved() is False
    assert game.is_game_over() is True


def test_it_keeps_track_of_invalid_guesses(game: Hangman):
    expected_tries_left = 3
    assert game.get_tries_left() == expected_tries_left
    for i in range(1, 4):
        game.guess("x")
        expected_tries_left -= 1
        assert game.get_tries_left() == expected_tries_left

    assert game.get_tries_left() == 0
    with pytest.raises(RuntimeError) as exception:
        game.guess("x")
    assert str(exception.value) == "Game is over. Cannot guess anymore."


def test_it_renders_the_word_mask_correctly(game: Hangman):
    expected_mask = len("cake") * "_"
    assert game.get_mask() == expected_mask

    game.guess("c")
    expected_mask = "c" + expected_mask[1:]
    assert game.get_mask() == expected_mask

    game.guess("a")
    expected_mask = "ca" + expected_mask[2:]
    assert game.get_mask() == expected_mask

    game.guess("e")
    expected_mask = "ca" + expected_mask[2:-1] + "e"
    assert game.get_mask() == expected_mask

    game.guess("k")
    expected_mask = "cake"
    assert game.get_mask() == expected_mask
