"""Unit tests of the cli runner
"""
from io import StringIO

import sys
import pytest
import hangman.cli_runner as runner
from _pytest.capture import CaptureFixture
from hangman.game import Hangman
from unittest.mock import Mock
from contextlib import contextmanager


@contextmanager
def replace_stdin(target):
    orig = sys.stdin
    sys.stdin = target
    yield
    sys.stdin = orig


@pytest.fixture
def game() -> Mock:
    """Return a game mock.
    :return: Mock of Hangman
    """
    return Mock(spec=Hangman)


def test_it_will_allow_the_user_to_win(game: Mock, capsys: CaptureFixture):
    # mock game calls in a successful play-through:
    game.is_game_over.side_effect = [False, True]
    game.get_mask.side_effect = [['_'], ['c']]
    game.get_tries_left.side_effect = [1, 1]
    game.guess.return_value = True
    game.is_solved.return_value = True
    # simulate user input of:
    # "y" to start the game
    # "c" to guess "c"
    # "n" to end the game by breaking the game loop
    with replace_stdin(StringIO("y\nc\nn")):
        runner.start_the_game(game)
    game.guess.assert_called_once_with('c')
    output = capsys.readouterr().out.split('\n')

    assert len(output) == 6
    assert output[1] == 'Guess a single letter: Your guess is "c".'
    assert output[2] == 'You have guessed correctly!'
    assert output[4] == 'You have won 1 out of 1 attempt(s)'


def test_it_will_allow_the_user_to_lose(game: Mock, capsys: CaptureFixture):
    # mock game calls in an unsuccessful play-through:
    game.is_game_over.side_effect = [False, True]
    game.get_mask.side_effect = [['_'], ['_']]
    game.get_tries_left.side_effect = [1, 0]
    game.guess.return_value = False
    game.is_solved.return_value = False
    # simulate user input of:
    # "y" to start the game
    # "x" to incorrectly guess "x"
    # "n" to end the game by breaking the game loop
    with replace_stdin(StringIO("y\nx\nn")):
        runner.start_the_game(game)
    game.guess.assert_called_once_with('x')
    output = capsys.readouterr().out.split('\n')

    assert len(output) == 6
    assert output[1] == 'Guess a single letter: Your guess is "x".'
    assert output[2] == 'You have guessed incorrectly'
    assert output[4] == 'You have won 0 out of 1 attempt(s)'


def test_it_can_process_users_empty_guesses():
    prompt_message = "enter a letter\n"
    # expect an EOFError because the method will re-try to get
    # user's input if he supplies an empty string:
    with pytest.raises(EOFError):
        with replace_stdin(StringIO("\n")):
            runner.process_users_guess(prompt_message)


def test_it_can_process_users_incorrect_guesses(capsys: CaptureFixture):
    words = "cannot_pass_words"
    prompt_message = "enter a letter\n"
    # expect an EOFError because the method will re-try to get
    # user's input if he supplies an empty string:
    with pytest.raises(EOFError):
        with replace_stdin(StringIO(words)):
            runner.process_users_guess(prompt_message)
    output = capsys.readouterr().out.split('\n')
    assert len(output) == 4
    # remove newline char from prompt:
    assert output[0] == prompt_message[:-1]
    assert output[1] == f"You can only guess a single letter at a time. Your tried: {words}. Try again."
    assert output[2] == prompt_message[:-1]
